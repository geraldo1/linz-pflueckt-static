function loadMap() {
	var tiles = L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/light_all/{z}/{x}/{y}.png', {
		minZoom: 5,
		maxZoom: 20,
		attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, © <a href="https://carto.com/about-carto/">CARTO</a>',
	});
	var center = new L.LatLng(48.30, 14.30);

	var map = new L.Map('map', {center: center, zoom: 13, layers: [tiles]});
	map.attributionControl.setPrefix('Datenquelle: Stadt Linz, data.linz.gv.at');

	var markers = new L.MarkerClusterGroup();

	var greenIcon = L.icon({
		iconUrl: 'leaflet073/images/leaf-green.png',
		shadowUrl: 'leaflet073/images/leaf-shadow.png',
		iconSize:     [38, 95],
		shadowSize:   [50, 64],
		iconAnchor:   [22, 94],
		shadowAnchor: [4, 62],
		popupAnchor:  [-3, -76]
	});
	var apfelIcon = L.icon({
		iconUrl: 'leaflet073/images/leaf-Apfel.png',
		shadowUrl: 'leaflet073/images/leaf-shadow.png',
		iconSize:     [38, 95],
		shadowSize:   [50, 64],
		iconAnchor:   [22, 94],
		shadowAnchor: [4, 62],
		popupAnchor:  [-3, -76]
	});
	var birneIcon = L.icon({
		iconUrl: 'leaflet073/images/leaf-Birne.png',
		shadowUrl: 'leaflet073/images/leaf-shadow.png',
		iconSize:     [38, 95],
		shadowSize:   [50, 64],
		iconAnchor:   [22, 94],
		shadowAnchor: [4, 62],
		popupAnchor:  [-3, -76]
	});
	var kastanieIcon = L.icon({
		iconUrl: 'leaflet073/images/leaf-Edelkastanie.png',
		shadowUrl: 'leaflet073/images/leaf-shadow.png',
		iconSize:     [38, 95],
		shadowSize:   [50, 64],
		iconAnchor:   [22, 94],
		shadowAnchor: [4, 62],
		popupAnchor:  [-3, -76]
	});
	var kirscheIcon = L.icon({
		iconUrl: 'leaflet073/images/leaf-Kirsche.png',
		shadowUrl: 'leaflet073/images/leaf-shadow.png',
		iconSize:     [38, 95],
		shadowSize:   [50, 64],
		iconAnchor:   [22, 94],
		shadowAnchor: [4, 62],
		popupAnchor:  [-3, -76]
	});
	var nuesseIcon = L.icon({
		iconUrl: 'leaflet073/images/leaf-Nüsse.png',
		shadowUrl: 'leaflet073/images/leaf-shadow.png',
		iconSize:     [38, 95],
		shadowSize:   [50, 64],
		iconAnchor:   [22, 94],
		shadowAnchor: [4, 62],
		popupAnchor:  [-3, -76]
	});
	var zwetschkeIcon = L.icon({
		iconUrl: 'leaflet073/images/leaf-Zwetschke.png',
		shadowUrl: 'leaflet073/images/leaf-shadow.png',
		iconSize:     [38, 95],
		shadowSize:   [50, 64],
		iconAnchor:   [22, 94],
		shadowAnchor: [4, 62],
		popupAnchor:  [-3, -76]
	});

	for (var i = 0; i < obstPoints.length; i++) {
		var a = obstPoints[i];
		if (a[10]=='Apfel') var baumIcon = apfelIcon;
		else if (a[10]=='Birne') var baumIcon = birneIcon;
		else if (a[10]=='Edelkastanie') var baumIcon = kastanieIcon;
		else if (a[10]=='Kirsche') var baumIcon = kirscheIcon;
		else if (a[10]=='Nüsse') var baumIcon = nuesseIcon;
		else if (a[10]=='Zwetschke') var baumIcon = zwetschkeIcon;
		else var baumIcon = greenIcon;
		var marker = new L.Marker(new L.LatLng(a[2], a[3]), { icon: baumIcon });
		marker.bindPopup("<b>"+a[1]+"</b><br><br>Gattung: "+a[4]+"<br>Art: "+a[5]+"<br>Sorte: "+a[6]+"<br>Kategorie: "+a[10]+"<br><br>Höhe: "+a[7]+" m<br>Schirmdurchmesser: "+a[8]+" m<br>Stammumfang: "+a[9]+" cm").openPopup();
		markers.addLayer(marker);
	}
	map.addLayer(markers);
	L.control.locate({
		title: "Zeige mir meinen aktuellen Standpunkt",
		popupText: ["Du bist im Umkreis von ", " von diesem Punkt."],
	}).addTo(map);

	var popup = L.popup();
	function onMapClick(e) {
		popup
		    .setLatLng(e.latlng)
		    .setContent("You clicked the map at " + e.latlng.toString())
		    .openOn(map);
	}
	map.on('click', onMapClick);
}

