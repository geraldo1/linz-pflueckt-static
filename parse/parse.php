<?php
/*
 * Read csv file and write content to geojson file
**/
$fin = 'FME_BaumdatenBearbeitet_OGD_20200806.csv';
$onlyObst = true;

// filter and mapping Name -> Kategorie
$KategorieArray = [
	'Apfel' => 'Apfel',
	'Apfelquitte' => 'Sonstiges',
	'Apfelquitte' => 'Sonstiges',
	'Baumhasel' => 'Nüsse',
	'Birne' => 'Birne',
	'Birnenquitte' => 'Sonstiges',
	'Birnequitte' => 'Sonstiges',
	'Blut-Pflaume' => 'Zwetschke',
	'Breitblättrige Mehlbeere' => 'Sonstiges',
	'Chinesische Wildbirne' => 'Birne',
	'Chinesischer Apfel' => 'Apfel',
	'Christusdorn' => 'Sonstiges',
	'Dreilappige Indianerbanane' => 'Sonstiges',
	'Eberesche' => 'Sonstiges',
	'Eberesche "Autumn Spire"' => 'Sonstiges',
	'Echte Mehlbeere' => 'Sonstiges',
	'Edelkastanie' => 'Edelkastanie',
	'Eingriffliger Weißdorn' => 'Sonstiges',
	'Elsbeere' => 'Sonstiges',
	'Essigbaum' => 'Sonstiges',
	'Felsenbirne' => 'Birne',
	'Flügelnuss' => 'Nüsse',
	'Fruchkirsche' => 'Kirsche',
	'Fruchtkirsche' => 'Kirsche',
	'Fruchtmaroni' => 'Edelkastanie',
	'Fruchtpflaume' => 'Zwetschke',
	'Fruchtweichsel' => 'Kirsche',
	'Fruchtzwetschke' => 'Zwetschke',
	'Fruchweichsel' => 'Kirsche',
	'Fudschijama-Kirsche' => 'Kirsche',
	'Gefülltblühende Vogel-Kirsche' => 'Kirsche',
	'Goldfrüchtiger Weißdorn' => 'Sonstiges',
	'Götterbaum' => 'Sonstiges',
	'Großblütige Trauben-Kirsche' => 'Kirsche',
	'Große Walnuss' => 'Nüsse',
	'Großfrüchtiger Wacholder' => 'Sonstiges',
	'Hänge-Kirsche' => 'Kirsche',
	'Hasel' => 'Nüsse',
	'Herbstblühende Schnee-Kirsche' => 'Kirsche',
	'Higan Kirsche' => 'Kirsche',
	'Japan. Blüten-Kirsche' => 'Kirsche',
	'Japan. Säulen-Kirsche' => 'Kirsche',
	'Japanische Alpen-Kirsche' => 'Kirsche',
	'Japanische Aprikose' => 'Sonstiges',
	'Japanische Blüten-Kirsche' => 'Kirsche',
	'Kakibaum' => 'Sonstiges',
	'Kanadische Felsenbirne' => 'Birne',
	'Kaukasische Flügelnuss' => 'Nüsse',
	'Kirsch-Pflaume' => 'Zwetschke',
	'Kirsche' => 'Kirsche',
	'Krieche' => 'Zwetschke',
	'Kugel-Steppen-Kirsche' => 'Kirsche',
	'Kulturapfel' => 'Apfel',
	'Kulturbirne' => 'Birne',
	'Kulurapfel' => 'Apfel',
	'Kupfer - Felsenbirne' => 'Birne',
	'Mährische Eberesche' => 'Sonstiges',
	'Mandelbaum' => 'Nüsse',
	'Marille' => 'Sonstiges',
	'Maulbeerbaum' => 'Sonstiges',
	'Mehl-/Vogelbeere'=> 'Sonstiges',
	'Mirabelle' => 'Zwetschke',
	'Mispel' => 'Sonstiges',
	'Mostbirne' => 'Birne',
	'Nashi-Birne' => 'Birne',
	'Nelken-Kirsche' => 'Kirsche',
	'Nuss' => 'Nüsse',
	'Österreichische Mehlbeere' => 'Sonstiges',
	'Pekanuss' => 'Nüsse',
	'Pfirsich' => 'Sonstiges',
	'Pflaumenblättriger Weißdorn' => 'Sonstiges',
	'Platanenblättriger Maulbeerbaum' => 'Sonstiges',
	'Portugiesische Lorbeer-Kirsche' => 'Kirsche',
	'Quitte' => 'Sonstiges',
	'Ringlotte' => 'Zwetschke',
	'Robinie' => 'Sonstiges',
	'Rosarote Akazie' => 'Sonstiges',
	'Rosen-Akazie' => 'Sonstiges',
	'Rotblättrige Trauben-Kirsche' => 'Kirsche',
	'Rotdorn' => 'Sonstiges',
	'Roter Maulbeerbaum' => 'Sonstiges',
	'Sanddorn' => 'Sonstiges',
	'Säulen-Robinie' => 'Sonstiges',
	'Säulenwacholder' => 'Sonstiges',
	'Schlehdorn' => 'Zwetschke',
	'Schwarzer Holler' => 'Sonstiges',
	'Schwarzer Maulbeerbaum' => 'Sonstiges',
	'Schwarznuss' => 'Nüsse',
	'Schwedische Mehlbeere' => 'Sonstiges',
	'Spanischer Wacholder' => 'Sonstiges',
	'Späte Trauben-Kirsche' => 'Kirsche',
	'Stein-Weichsel' => 'Kirsche',
	'Thüringische Mehlbeere' => 'Sonstiges',
	'Thüringische Säulen-Mehlbeere' => 'Sonstiges',
	'Tibet-Kirsche' => 'Kirsche',
	'Trauben-Kirsche' => 'Kirsche',
	'Ussuri-Birne' => 'Birne',
	'Virginische Rotblättrige Trauben-Kirsche' => 'Kirsche',
	'Vogel-Kirsche' => 'Kirsche',
	'Vogelbeere' => 'Sonstiges',
	'Walnuss' => 'Nüsse',
	'Weißdorn' => 'Sonstiges',
	'Weißer Maulbeerbaum' => 'Sonstiges',
	'Wild-Apfel' => 'Apfel',
	'Wildapfel' => 'Apfel',
	'Wildbirne' => 'Birne',
	'Wildfpflaume' => 'Zwetschke',
	'Wildkirsche' => 'Kirsche',
	'Wildpflaume' => 'Zwetschke',
	'Woll-Apfel' => 'Apfel',
	'Yoshino-Kirsche' => 'Kirsche',
	'Zweigriffliger Weißdorn' => 'Sonstiges',
	'Zwetschke' => 'Zwetschke',
]; 

//write to geojson file
//format: ["name", long, lat],
if ($onlyObst)
	$foutput = fopen('obst.js', 'w');
else
	$foutput = fopen('alle.js', 'w');
fwrite($foutput, 'var obstPoints = ['.PHP_EOL);

//open .csv file and read line by line
//format OLD: Fläche;BaumNr;Gattung;Art;Sorte;NameDeutsch;Höhe;Schirmdurchmesser;Stammumfang;Typ;XPos;YPos;Kategorie;Reifvon;Reifbis

//format NEW: Flaeche;BaumNr;Gattung;Art;Sorte;NameDeutsch;Hoehe;Schirmdurchmesser;Stammumfang;Typ;XPos;YPos;lon;lat

$lines = file($fin);
foreach ($lines as $n => $line) {
	if ($n > 1) {
		$tokens = explode(';',$line);
		$flaeche = trim($tokens[0]);
		$baumnr = trim($tokens[1]);
		$gattung = trim($tokens[2]);
		$art = trim($tokens[3]);
		if ($art == '.') $art = '';
		$sorte = trim($tokens[4]);
		if ($sorte == '.') $sorte = '';
		$sorte = str_replace('\'', '', $sorte);

		$title = trim($tokens[5]);
		$title = str_replace('"', '', $title);
		$title = str_replace('\'', '', $title);

		$hoehe = trim($tokens[6]);
		$schirm = trim($tokens[7]);
		$stamm = trim($tokens[8]);
		$lat = trim($tokens[12]);
		$long = trim($tokens[13]);
		//$date1 = trim($tokens[13]);
		//$date2 = trim($tokens[14]);

		if ($onlyObst) {
			foreach ($KategorieArray as $kat => $kategorie) {
				if (startsWith($title, $kat)) {

					fwrite($foutput, "[".$n.", '".$title."', ".$long.", ".$lat.", '".$gattung."', '".$art."', '".$sorte."', ".$hoehe.", ".$schirm.", ".$stamm.", '".$kategorie."', '".$flaeche."', '".$baumnr."'],".PHP_EOL);
				}
			}

			/*if (isset($KategorieArray[$title])) {
				$kat = $KategorieArray[$title];

				fwrite($foutput, "[".$n.", '".$title."', ".$long.", ".$lat.", '".$gattung."', '".$art."', '".$sorte."', ".$hoehe.", ".$schirm.", ".$stamm." '".$kat."', '".$flaeche."', '".$baumnr."'],".PHP_EOL);
			}*/
		}
		else {
			fwrite($foutput, "[".$n.", '".$title."', ".$long.", ".$lat.", '".$gattung."', '".$art."', '".$sorte."', ".$hoehe.", ".$schirm.", ".$stamm.", '".$flaeche."', '".$baumnr."'],".PHP_EOL);
		}
		
		//echo "<p>".htmlentities($title)." [".$long."/".$lat."]</p>";

		//fwrite($foutput, "[".$n.", '".$title."', ".$long.", ".$lat.", '".$gattung."', '".$art."', '".$sorte."', ".$hoehe.", ".$schirm.", ".$stamm.", '".$kat."', '".$date1."', '".$date2."'],".PHP_EOL);
	}
}

//stop to write geojson file
fwrite($foutput, '];'.PHP_EOL);

echo $n." datasets written to geojson file";

function startsWith( $haystack, $needle ) {
     $length = strlen( $needle );
     return substr( $haystack, 0, $length ) === $needle;
}

?>
